package com.example.ady.rssreader;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ady on 11.02.2016.
 */
public class RSSDBHandler extends SQLiteOpenHelper{

    private static final String DATABASE_NAME = "rssReader";
    private static final int DATABASE_VERSION = 1;

    //websites table name
    private static final String TABLE_RSS = "websites";

    //Websites table column names
    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_LINK = "link";
    private static final String KEY_RSS_LINK = "rss_link";
    private static final String KEY_DESCRIPTION = "description";

    public RSSDBHandler(Context ctx){
        super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_RSS_TABLE = "CREATE TABLE " + TABLE_RSS + "(" + KEY_ID
                + " INTEGER PRIMARY KEY," + KEY_TITLE + " TEXT,"
                + KEY_LINK + " TEXT," + KEY_RSS_LINK + " TEXT," + KEY_DESCRIPTION + " TEXT" + ")";
        db.execSQL(CREATE_RSS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RSS);
        onCreate(db);
    }

    /**
     * Adding a new website in websites table Function will check if a site
     * already existed in database. If existed will update the old one else
     * creates a new row
     * */
    public void addSite(Website site){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, site.getTitle());
        values.put(KEY_LINK, site.getLink());
        values.put(KEY_RSS_LINK, site.getRSSLink());
        values.put(KEY_DESCRIPTION, site.getDescription());

        //check if row already exists in db
        if(!isSiteExists(db,site.getRSSLink())){
            //site not existed, create a new row
            db.insert(TABLE_RSS,null,values);
            db.close();
        } else {
            //site already exists, update the row
            updateSite(site);
            db.close();
        }
    }

    //returns all the rows as Website class object
    public List<Website> getAllSites(){
        List<Website> siteList = new ArrayList<Website>();
        //Select all query
        String selectQ = "SELECT * FROM " + TABLE_RSS + " ORDER BY id DESC";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQ,null);

        if(cursor.moveToFirst()){
            do {
                Website site = new Website();
                site.setId(Integer.parseInt(cursor.getString(0)));
                site.setTitle(cursor.getString(1));
                site.setLink(cursor.getString(2));
                site.setRSSLink(cursor.getString(3));
                site.setDescription(cursor.getString(4));
                //adding contact to list
                siteList.add(site);
            }
            while (cursor.moveToNext())
        }
        cursor.close();
        db.close();

        return siteList;
    }

    //update existing row
    public int updateSite(Website site){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(KEY_TITLE, site.getTitle());
        val.put(KEY_LINK, site.getLink());
        val.put(KEY_RSS_LINK, site.getRSSLink());
        val.put(KEY_DESCRIPTION,site.getDescription());

        //updating row return
        int update = db.update(TABLE_RSS, val, KEY_RSS_LINK + " = ?",new String[]{String.valueOf(site.getRSSLink())});
        db.close();
        return update;
    }

    //returns single site
    public Website getSite(int id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cur = db.query(TABLE_RSS, new String[]{ KEY_ID, KEY_TITLE, KEY_LINK, KEY_RSS_LINK, KEY_DESCRIPTION},
                KEY_ID + " = ?", new String[]{ String.valueOf(id)}, null, null, null, null);
        if(cur != null)
            cur.moveToFirst();

        Website site = new Website(cur.getString(1),cur.getString(2), cur.getString(3), cur.getString(4));

        site.setId(Integer.parseInt(cur.getString(0)));
        site.setTitle(cur.getString(1));
        site.setLink(cur.getString(2));
        site.setRSSLink(cur.getString(3));
        site.setDescription(cur.getString(4));
        cur.close();
        db.close();
        return site;
    }

    //deletes single row
    public void deleteSite(Website site){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_RSS, KEY_ID + " = ?", new String[]{ String.valueOf(site.getID())});
        db.close;
    }

    /**
     * Checking whether a site is already existed check is done by matching rss
     * link
     * */
    public boolean isSiteExists(SQLiteDatabase db, String rss_link){
        Cursor cur = db.rawQuery("SELECT 1 FROM " + TABLE_RSS + "WHERE rss_link='"+ rss_link + "'",new String[]{});
        boolean exists = (cur.getCount() > 0);
        return exists;
    }

}
