package com.example.ady.rssreader;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by ady on 11.02.2016.
 */
public class AddNewSiteActivity extends AppCompatActivity {
    Button btnSubmit;
    Button btnCancel;
    EditText txtUrl;
    TextView lblMessage;

    RSSParser rssParser = new RSSParser();

    RSSFeed rssFeed;
    private ProgressDialog pgDialog;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_site);

        //buttons
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        txtUrl = (EditText) findViewById(R.id.txtUrl);
        lblMessage = (TextView) findViewById(R.id.lblMessage);

        //Submit buttons click event
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = txtUrl.getText().toString();

                //Validation url
                Log.d("URL length:", "" + url.length());
                //check if user entered any data in EditText
                if(url.length() > 0){
                    lblMessage.setText("");
                    String urlPattern = "^http(s{0,1})://[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*";
                    if(url.matches(urlPattern)){
                        //valid Url
                        new loadRSSFeed().execute(url);
                    } else {
                        //Url not valid
                        lblMessage.setText("Please enter a valid url:");
                    }
                } else {
                    //please enter a url
                    lblMessage.setText("Please enter website url");
                }
            }
        });

        //cancel button click event
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * Background Async Task to get RSS data from url
     */
    class loadRSSFeed extends AsyncTask<String, String, String> {
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pgDialog = new ProgressDialog(AddNewSiteActivity.this);
            pgDialog.setMessage("Fetching RSS information");
            pgDialog.setIndeterminate(false);
            pgDialog.setCancelable(false);
            pgDialog.show();
        }

        /**
         * Getting Inbox JSON
         */
        @Override
        protected String doInBackground(String... args) {
            String url = args[0];
            rssFeed = rssParser.getRSSFeed(url);
            Log.d("rssFeed"," "+ rssFeed);
            if(rssFeed!= null){
                Log.e("RSS URL",
                        rssFeed.getTitle() + " " + rssFeed.getLink() + " "+
                                rssFeed.getDescription() + " " +
                                rssFeed.getLanguage());

                RSSDBHandler rssDb = new RSSDBHandler(getApplicationContext());
                Website site = new Website(rssFeed.getTitle(),rssFeed.getLink(),
                        rssFeed.getRSSLink()t rssFeed.getDescription());
                rssDb.addSite(site);
                Intent i = getIntent();
                //send result code 100 to notify about website update
                setResult(100,i);
                finish();
            } else {
                //updating UI from Background Thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        lblMessage.setText("Rss url not found.Please check the url or try again");
                    }
                });
            }
            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         */
        @Override
        protected void onPostExecute(String s) {
            //dismiss the dialog after getting all links
            pgDialog.dismiss();
            //updating UI from Background Thread
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(rssFeed != null){

                    }
                }
            });

        }
    }
}
