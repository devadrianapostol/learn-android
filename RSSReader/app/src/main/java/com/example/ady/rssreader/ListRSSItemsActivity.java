package com.example.ady.rssreader;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ady on 11.02.2016.
 */
public class ListRSSItemsActivity extends ListActivity
{
    //progress dialog
    private ProgressDialog pDialog;

    //Array list for list view
    ArrayList<HashMap<String,String>> rssItemList = new ArrayList<RSSItem>();

    RSSParser rssParser = new RSSParser();

    List<RSSItem> rssItems = new ArrayList<RSSItem>();

    RSSFeed rssFeed;


    private static String TAG_TITLE = "title";
    private static String TAG_LINK = "link";
    private static String TAG_DESCRIPTION = "description";
    private static String TAG_PUB_DATE = "pubDate";
    private static String TAG_GUID = "guid"; //not used

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rss_item_list);

        //get intent data
        Intent i = getIntent();

        //SQLite row id
        Integer site_id = Integer.parseInt(i.getStringExtra("id"));

        //Getting single website from SQLite
        RSSDBHandler rssDb = new RSSDBHandler(getApplicationContext());

        Website site = rssDb.getSite(site_id);
        String rss_linlk = site.getRSSLink();

        /**
         * Calling a Backgroun Thread will load recent articles of a website
         * @param String rss url of website
         */
        new loadRSSFeedItems().execute(rss_linlk);

        //selecting single ListView item
        ListView lv = getListView();

        //Launching new screen on Selecting Single ListItem
        lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent in = new Intent(getApplicationContext(),DisplayWebPageActivity.class);
                //getting page url
                String page_url = ((TextView) view.findViewById(R.id.page_url).getText().toString());
                Toast.makeText(getApplicationContext(),page_url, Toast.LENGTH_SHORT).show();
                in.putExtra("page_url", page_url);
                startActivity(in);
            }
        });

    }

    /**
     * Backgroung Async Task to get RSSFeed Items data from url
     */
    class loadRSSFeedItems extends AsyncTask<String, String , String> {
        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ListRSSItemsActivity.this);
            pDialog.setMessage("Loading recent articles...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * Getting all recent articles and showing them in listview
         */
        @Override
        protected String doInBackground(String... args) {
            //rss link url
            String rss_url = args[0];

            //list of rss items
            rssItems = rssParser.getRSSFeedItems(rss_url);

            //looping through each item
            for (RSSItem item : rssItems ) {
                //creating new HashMap
                HashMap<String, String> map = new HashMap<String,String>();

                //adding each child node to HashMap key=>value
                map.put(TAG_TITLE, item.getTitle());
                map.put(TAG_LINK, item.getLink());
                map.put(TAG_PUB_DATE, item.getPubdate());
                String description = item.getDescription();
                //taking only 200 chars from description
                if(description.length() > 100){
                    description = description.substring(0, 97) + "..";
                }
                map.put(TAG_DESCRIPTION, description);

                //adding HashList to ArrayList
                rssItems.add(map);
            }
            //updating UI from Background Thread
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    /**
                     * Updating parsed items into listview
                     */
                    ListAdapter adapter = new SimpleAdapter(ListRSSItemsActivity.this,
                            rssItemList, R.layout.rss_item_list_row,
                            new String[]{ TAG_LINK, TAG_TITLE, TAG_PUB_DATE, TAG_DESCRIPTION},
                            new int[] { R.id.page_url, R.id.title, R.id.pubdate, R.id.link});
                    setListAdapter(adapter);
                }
            });
            return null;
        }

        /**
         * After completing background task Dissmiss the Progress Dialog box
         */
        @Override
        protected void onPostExecute(String s) {
            pDialog.dismiss();
        }
    }
}
