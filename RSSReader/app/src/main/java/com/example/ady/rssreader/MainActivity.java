package com.example.ady.rssreader;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ProgressDialog pgDialog;

    //ArrayList for list view
    ArrayList<HashMap<String,String>> rssFeedList;

    RSSParser rssParser = new RSSParser();
    RSSFeed rssFeed;

    //button add new website
    ImageButton btnAddSite;

    //array to trace sqlite ids
    String[] sqliteIds;

    public static String TAG_ID = "id";
    public static String TAG_TITLE = "title";
    public static String TAG_LINK = "link";

    //ListView
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnAddSite = (ImageButton) findViewById(R.id.btnAddSite);

        //HashMap for ListView
        rssFeedList = new ArrayList<HashMap<String, String>>();

        /**
         * Calling a background thread which will load
         * web sites stored in SQLite database
         * */
        new loadStoredSites().execute();

        //selecting single ListView Item
        lv = (ListView) findViewById(R.id.list);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //getting values from selected ListItem
                String sqlite_id = ((TextView)view.findViewById(R.id.sqlite_id)).getText().toString();

                //Starting new Intent
                Intent in = new Intent(getApplicationContext(),ListRSSItemsActivity.class);
                //passing sqlite row id
                in.putExtra(TAG_ID, sqlite_id);
                startActivity(in);
            }
        });

        /**
         * Add new website button click event listener
         */
        btnAddSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),AddNewSiteActivity.class);
                // starting new activity and expecting some response back
                // depending on the result will decide whether new website is
                // added to SQLite database or not
                startActivityForResult(i, 100);
            }
        });
    }



    /**
     * Response from AddNewSiteActivity.java
     * if response is 100 means new site is added to sqlite
     * reload this activity again to show
     * newly added website in listview
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //if result code is 100
        if(resultCode == 100){
            //reload this screen again
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }
    }

    /**
     * Building a context menu for listview
     *Long press on List row to see context menu
     * @param menu
     * @param v
     * @param menuInfo
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if(v.getId() == R.id.list){
            menu.setHeaderTitle("Delete");
            menu.add(Menu.NONE, 0, 0, "Delete Feed");
        }
    }

    /**
     * Responding to context menu selected option
     * @param item
     * @return
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        //check for selected option
        if(menuItemIndex == 0){
            //user selected delete
            //delete the feed
            RSSDBHandler rssDB = new RSSDBHandler(getApplicationContext());
            Website site = new Website();
            site.setId(Integer.parseInt(sqliteIds[info.position]));
            rssDB.deleteSite(site);
            //reloading same activity again
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Background Async Task to get RSS data from url
     */
    class loadStoredSites  extends AsyncTask<String, String, String> {
        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pgDialog = new ProgressDialog(MainActivity.this);
            pgDialog.setMessage("Loading websites...");
            pgDialog.setIndeterminate(false);
            pgDialog.setCancelable(false);
            pgDialog.show();
        }

        /**
         * Getting all stored websites from SQLite
         */
        @Override
        protected String doInBackground(String... args) {
            //Updating UI from Background Thread
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    RSSDBHandler rssDb = new RSSDBHandler(getApplicationContext());
                    //listing all websites from db
                    List<Website> siteList = rssDb.getAllSites();
                    sqliteIds = new String[siteList.size()];

                    //loop through each website
                    for(int i=0; i<siteList.size();i++){
                        Website s = siteList.get(i);

                        //creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        //adding each child node to HashMap key => value
                        map.put(TAG_ID, s.getID().toString());
                        map.put(TAG_TITLE, s.getTitle());
                        map.put(TAG_LINK,s.getLink());

                        //adding HashList to ArrayList
                        rssFeedList.add(map);

                        //adding sqlite id to array
                        //used when deleting a website from sqlite
                        sqliteIds[i] = s.getID().toString();
                    }
                    /**
                     * Updating list view with websites
                     */
                    ListAdapter adapter = new SimpleAdapter(
                            MainActivity.this,
                            rssFeedList, R.layout.site_list_row,
                            new String[]{TAG_ID, TAG_TITLE, TAG_LINK},
                            new int[]{ R.id.sqlite_id, R.id.title, R.id.link}
                    );
                    //updating listview
                    lv.setAdapter(adapter);
                    registerForContextMenu(lv);
                }
            });

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String s) {
            pgDialog.dismiss();
        }
    }
}
