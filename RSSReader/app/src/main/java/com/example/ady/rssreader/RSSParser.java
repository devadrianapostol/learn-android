package com.example.ady.rssreader;

import android.util.Log;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 */
public class RSSParser {

    //RSS XML document CHANNEL tag
    private static String TAG_CHANNEL = "channel";
    private static String TAG_TITLE = "title";
    private static String TAG_LINK = "link";
    private static String TAG_DESCRIPTION = "description";
    private static String TAG_LANGUAGE  = "language";
    private static String TAG_ITEM = "item";
    private static String TAG_PUB_DATE = "pubDate";
    private static String TAG_GUID = "guid";

    //constructor
    public RSSParser(){

    }

    /**
     * Get RSS Feed from url
     *  @param url - is url of the website
     *  @return RSSFeed class object
     */
    public RSSFeed  getRSSFeed(String url){
        RSSFeed rssFeed = null;
        String rss_feed_xml = null;

        //getting rss link from html source code
        String rss_url = this.getRSSLinkFromUrl(url);
        
        //check if rss_link is found or not
        if(rss_url != null){
            //RSS url found
            //get RSS XML from rss url
            rss_feed_xml = this.getXmlFromUrl(rss_url);
            //check if rss_feed_xml is fetched or not
            if(rss_feed_xml != null){
                try {
                    Document doc = this.getDomElement(rss_feed_xml);
                    NodeList nodeList = doc.getElementsByTagName(TAG_CHANNEL);
                    Element e = (Element) nodeList.item(0);

                    //RSS nodes
                    String title = this.getValue(e, TAG_TITLE);
                    String link = this.getValue(e, TAG_LINK);
                    String description = this.getValue(e, TAG_DESCRIPTION);
                    String language = this.getValue(e, TAG_LANGUAGE);

                    //creating new RSS Feed
                    rssFeed = new RSSFeed(title,description, link, rss_url, language);
                } catch (Exception e){
                    //check log for errors
                    e.printStackTrace();
                }
            } else {
                //failed to fetch rss xml
            }
        } else {
            //no RSS url found
        }
        return RSSFeed;
    }

    /**
     * Getting RSSFeed items <item>
     * @param rss_url String - rss link url of the website
     * @return List<RSSItem> - List of RSSItem class objects
     */
    public List<RSSItem> getRSSFeedItems(String rss_url){
        List<RSSItem> itemsList = new ArrayList<RSSItem>();
        String rss_feed_xml;

        //get RSS XML from rss url
        rss_feed_xml = this.getXmlFromUrl(rss_url);

        //check if RSS XML is fetched or not
        if(rss_feed_xml != null){
            //successfully fetched rss xml
            //parse the xml
            try {
                Document doc = this.getDomElement(rss_feed_xml);
                NodeList nodeList = doc.getElementsByTagName(TAG_CHANNEL);
                Element e = (Element) nodeList.item(0);

                //getting items array
                NodeList items = e.getElementsByTagName(TAG_ITEM);
                
                //looping through each item
                for(int i = 0; i<items.getLength(); i++){
                    Element e1 = (Element) items.item(i);

                    String title = this.getValue(e1, TAG_TITLE);
                    String link = this.getValue(e1, TAG_LINK);
                    String description = this.getValue(e1, TAG_DESCRIPTION);
                    String pubdate = this.getValue(e1, TAG_PUB_DATE);
                    String guid = this.getValue(e1, TAG_GUID);

                    RSSItem rssItem = new RSSItem(title,link,description, pubdate,guid);
                    //adding item to list
                    itemsList.add(rssItem);
                }

            } catch (Exception e){
                e.printStackTrace();
            }

        }
        return itemsList;
    }

    /**
     * Getting node value
     * @param element Node - element
     * @return String
     */
    public final String getElementValue(Node element){
        Node child;
        if(element != null) {
            if(element.hasChildNodes()){
                for (child = element.getFirstChild(); child != null; child=element.getNextSibling()){
                    if (child.getNodeType() == Node.TEXT_NODE || (child.getNodeType() == Node.CDATA_SECTION_NODE)){
                        return child.getNodeValue();
                    }
                }
            }
        }
        return "";
    }
    /**
     * Getting node value
     * @param e Element
     * @param key String
     * @return
         */
    public String getValue(Element e, String key) {
        NodeList n = e.getElementsByTagName(key);
        return this.getElementValue(n.item(0));
    }

    /**
     * Getting XML Dom Element
     * @param xml String
     * @return Document
     */
    public Document getDomElement(String xml) {
        Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();

            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            doc = (Document) db.parse(is);
        } catch (ParserConfigurationException e){
            Log.e("Error :", e.getMessage());
            return null;
        } catch (SAXException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (IOException e){
            Log.e("Error: ", e.getMessage());
            return null;
        }

        return doc;
    }

    /**
     * Get XML content from url HTTP GET request
     * @param url String
     * @return String
     */
    public String getXmlFromUrl(String url) {
        String xml = null;

        try {
            //request method is GET
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet =  new HttpGet(url);

            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            xml = EntityUtils.toString(httpEntity);
        } catch (UnsupportedEncodingException e){
            e.printStackTrace();
        } catch (ClientProtocolException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
        //return xml
        return xml;
    }

    /**
     * Getting RSS feed link from url
     * @param url String - is url of the website
     * @return String - url of rss link of website
     */
    public String getRSSLinkFromUrl(String url) {
        //RSS url
        String rss_url = null;
        try {
            //using Jsoup library to parse HTML source
            org.jsoup.nodes.Document doc = Jsoup.connect(url).get();
            //finding rss links which are having link[type=application/rss+xml]
            org.jsoup.select.Elements links = doc.select("link[type=application/rss+xml]");
            Log.d("Nr of RSS links found:", " " + links.size());

            //check if urls found or not
            if(links.size() > 0){
                rss_url = links.get(0).attr("href").toString();
            } else {
                //finding rss links which are having link[type=application/atom+xml]
                org.jsoup.select.Elements links1 =  doc.select("link[type=application/atom+xml]");
                if(links1.size() > 0){
                    rss_url = links1.get(0).attr("href").toString();
                }
            }
            
        } catch (Exception e){
            e.printStackTrace();
        }
        //returning rss_url
        return rss_url;
    }

}
