package com.example.ady.sqliteapp;

/**
 * Created by ady on 04.02.2016.
 */
public class Contact {
    //private vars
    private int _id;
    private String _name;
    private String _phonenumber;

    public Contact(){

    }

    public Contact(int _id, String _name, String _phonenumber){
        this._id = _id;
        this._name = _name;
        this._phonenumber = _phonenumber;
    }

    public Contact(String _name, String _phonenumber){
        this._name = _name;
        this._phonenumber = _phonenumber;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_phonenumber() {
        return _phonenumber;
    }

    public void set_phonenumber(String _phonenumber) {
        this._phonenumber = _phonenumber;
    }

}
