package com.adrian.serviceone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Messenger;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView textView;
    private BroadcastReceiver receiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(bundle != null){
                String string = bundle.getString(ServiceOne.FILEPATH);
                int resultCode = bundle.getInt(ServiceOne.RESULT);
                if(resultCode == RESULT_OK){
                    //Toast.makeText(MainActivity.this,"Download complete.Download URI: " + string,
                            //Toast.LENGTH_LONG).show();
                    textView.setText("Download done!");
                }
                else {
                    //Toast.makeText(MainActivity.this,"Download failed",Toast.LENGTH_LONG).show();
                    textView.setText("Download failed");
                }
            }
        }
    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.status);
    }


    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(ServiceOne.NOTIFICATION));
    }


    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    public void onClick(View view){
        Intent i = new Intent(this, ServiceOne.class);
        //add infos for the service which file to download and where to store
        i.putExtra(ServiceOne.FILENAME,"index.html");
        i.putExtra(ServiceOne.URL, "http://www.vogella.com/index.html");
        startService(i);
        textView.setText("Service started");
    }
}
