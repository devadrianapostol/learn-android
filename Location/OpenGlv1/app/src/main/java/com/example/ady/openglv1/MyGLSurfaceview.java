package com.example.ady.openglv1;

import android.content.Context;
import android.opengl.GLSurfaceView;

/**
 * Created by ady on 25.02.2016.
 */
public class MyGLSurfaceview extends GLSurfaceView {
    private final MyGLRenderer mRenderer;

    public MyGLSurfaceview(Context context) {
        super(context);

        //create a OpenGL ES 2.0 context
        setEGLContextClientVersion(2);

        mRenderer = new MyGLRenderer();

        //set the renderer for drawing on the GLSurfaceView
        setRenderer(mRenderer);

        // Render the view only when there is a change in the drawing data
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }
}
