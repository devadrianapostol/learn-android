package com.example.ady.todosqlite;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.example.ady.todosqlite.com.example.ady.todosqlite.model.Tag;
import com.example.ady.todosqlite.com.example.ady.todosqlite.model.Todo;
import com.example.ady.todosqlite.helper.DBHelper;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    DBHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new  DBHelper(getApplicationContext());

        //Creating tags
        Tag tag1 = new Tag("Shopping");
        Tag tag2 = new Tag("Important");
        Tag tag3 = new Tag("Watchlist");
        Tag tag4 = new Tag("Androidhive");

        //Inserting tag in db
        long tag_id1 = db.createTag(tag1);
        long tag_id2 = db.createTag(tag2);
        long tag_id3 = db.createTag(tag3);
        long tag_id4 = db.createTag(tag4);

        Log.d("TAG COUNT", "Tag count " + db.getAllTags().size());

        //Creating Todos
        Todo todo1 = new Todo("iPhone 5S",0);
        Todo todo2 = new Todo("Galaxy Note II", 0);
        Todo todo3 = new Todo("Whiteboard",0);

        Todo todo4 = new Todo("Riddick", 0);
        Todo todo5 = new Todo("Prisoners", 0);
        Todo todo6 = new Todo("The Croods", 0);
        Todo todo7 = new Todo("Insidious: Chapter 2", 0);

        Todo todo8 = new Todo("Don't forget to call Mom", 0);
        Todo todo9 = new Todo("Collect money from John", 0);

        Todo todo10 = new Todo("Post new article", 0);
        Todo todo11 = new Todo("Take database backup", 0);

        //Inserting todos in db
        //Inserting todos under "Shopping" tag
        long todo1_id = db.createTodo(todo1, new long[]{ tag_id1});
        long todo2_id = db.createTodo(todo2, new long[]{ tag_id1});
        long todo3_id = db.createTodo(todo3, new long[] {tag_id1});

        //Inserting todos under "Watchlist" tag
        long todo4_id = db.createTodo(todo4, new long[]{tag_id3});
        long todo5_id = db.createTodo(todo5, new long[]{ tag_id3});
        long todo6_id = db.createTodo(todo5, new long[]{ tag_id3});
        long todo7_id = db.createTodo(todo7, new long[] {tag_id3});

        //Inserting todos under "Important" tag
        long todo8_id = db.createTodo(todo8, new long[]{ tag_id2});
        long todo9_id = db.createTodo(todo9, new long[]{ tag_id2});

        //Inserting todos under "Androidhive" tag
        long todo_id10 = db.createTodo(todo10, new long[]{tag_id4});
        long todo_id11 = db.createTodo(todo11, new long[]{tag_id4});

        Log.e("TODO COUNT", "Todo count: " + db.getTodoCount());

        //"Post new article - assign this under "Important" tag
        //Now this will have - "Androidhive" and "Important" tags
        db.createTodoTag(todo_id10, tag_id2);

        //Getting all tag names
        Log.d("Get Tags", "Getting all tags");

        List<Tag> allTags = db.getAllTags();
        for ( Tag tg : allTags  ) {
            Log.d("TAG NAME", tg.getTag_name());
        }

        //Getting all todos under "Watchlist" tag name
        Log.d("ToDo", "Get todos under single tag name");
        List<Todo> tagsWatchList = db.getAllTodosByTag(tag3.getTag_name());
        for (Todo td: tagsWatchList) {
            Log.d("ToDo Watchlist", td.getNote());
        }

        //Deleting a ToDo
        Log.d("Delete ToDo", "Deleting a todo");
        Log.d("Tag Count","Tag Count before deleting: " + db.getTodoCount());

        db.deleteTodo(todo8_id);

        Log.d("Tag Count", "Tag count after deleting: " + db.getTodoCount());

        //Deleting all todos under "Shopping" tag
        Log.d("Tag Count", "Tag Count before deleting 'Shopping' todos: " + db.getTodoCount());

        db.deleteTag(tag1, true);

        Log.d("Tag Count", "Tag Count after deleting 'Shopping' todos: " + db.getTodoCount());

        //Updating tag name
        tag3.setTag_name("Movies to watch");
        db.updateTag(tag3);

        //don't forget to close db connection
        db.closeDb();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
