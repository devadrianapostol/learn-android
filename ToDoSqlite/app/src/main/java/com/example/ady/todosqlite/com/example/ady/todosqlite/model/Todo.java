package com.example.ady.todosqlite.com.example.ady.todosqlite.model;

/**
 * Created by ady on 05.02.2016.
 */
public class Todo {
    int id;
    String note;
    int status;
    String created_at;

    //constructors
    public Todo(){

    }

    public Todo(String note,int status){
        this.note = note;
        this.status = status;
    }

    public Todo(int id, String note, int status){
        this.id = id;
        this.note = note;
        this.status = status;
    }

    //getters

    public int getId() {
        return id;
    }

    public String getNote() {
        return note;
    }

    public int getStatus() {
        return status;
    }

    //setters
    public void setNote(String note) {
        this.note = note;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setCreated_at(String created_at){
        this.created_at = created_at;
    }


}
