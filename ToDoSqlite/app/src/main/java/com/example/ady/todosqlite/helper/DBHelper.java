package com.example.ady.todosqlite.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.ady.todosqlite.com.example.ady.todosqlite.model.Tag;
import com.example.ady.todosqlite.com.example.ady.todosqlite.model.Todo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by ady on 05.02.2016.
 */
public class DBHelper extends SQLiteOpenHelper {
    //Logcat tag
    private static final String LOG = "DBHelper";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "todoManager";

    //table names
    private static final String TABLE_TODO = "todos";
    private static final String TABLE_TAG = "tags";
    private static final String TABLE_TODO_TAG = "todos_tags";

    //Common column names
    private static final String KEY_ID = "id";
    private static final String KEY_CREATED_AT = "created_at";

    //Notes Table - column names
    private static final String KEY_TODO = "todo";
    private static final String KEY_STATUS = "status";

    //Tags Table - column names
    private static final String KEY_TAG_NAME = "tag_name";

    //Note_Tags Table - column names
    private static final String KEY_TODO_ID = "todo_id";
    private static final String KEY_TAG_ID = "tag_id";

    /*
    Table Create statement
    Todo Table create statement
    */
    private static final String CREATE_TABLE_TODO = "CREATE TABLE "+ TABLE_TODO +
            "("+KEY_ID+" INTEGER PRIMARY KEY," + KEY_TODO + " TEXT," +
            KEY_STATUS + " INTEGER," + KEY_CREATED_AT + " DATETIME"+")";

    //tag table create statement
    /**
     * CREATE TABLE todo(id INTEGER PRIMARY KEY,todo TEXT,status INTEGER,created_at DATETIME);
     */
    private static final String CREATE_TABLE_TAG = "CREATE TABLE "+ TABLE_TAG
            +"("+KEY_ID + " INTEGER PRIMARY KEY," + KEY_TAG_NAME
            + " TEXT," + KEY_CREATED_AT + " DATETIME" + ")";

    //todo_tag table create statement
    private static final String CREATE_TABLE_TODO_TAG = "CREATE TABLE " + TABLE_TODO_TAG
            + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TODO_ID + " INTEGER,"
            + KEY_TAG_ID + " INTEGER," + KEY_CREATED_AT + " DATETIME" + ")";

    public DBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_TODO);
        db.execSQL(CREATE_TABLE_TAG);
        db.execSQL(CREATE_TABLE_TODO_TAG);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TODO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TAG);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TODO_TAG);

        //create new tables
        onCreate(db);
    }

    /**
     * creating a TODO
     */
    public long createTodo(Todo todo, long[] tag_ids){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TODO, todo.getNote());
        values.put(KEY_STATUS, todo.getStatus());
        values.put(KEY_CREATED_AT, getDateTime());

        //insert row
        long todo_id = db.insert(TABLE_TODO, null, values);

        //asigning tags to todo
        for (long tag_id: tag_ids ) {
            createTodoTag(todo_id, tag_id);
        }

        return todo_id;
    }

    private String getDateTime() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        return timeStamp;
    }

    /**
     * get single todo
     */
    public Todo getTodo(long todo_id){
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM "+TABLE_TODO + "WHERE "+ KEY_ID + " = " + todo_id;

        Log.e(LOG, selectQuery);

        Cursor cur = db.rawQuery(selectQuery, null);

        if(cur != null) {
            cur.moveToFirst();
        }

        Todo td = new Todo();
        td.setId(cur.getInt(cur.getColumnIndex(KEY_ID)));
        td.setNote(cur.getString(cur.getColumnIndex(KEY_TODO)));
        td.setCreated_at(cur.getString(cur.getColumnIndex(KEY_CREATED_AT)));

        return td;
    }

    /**
     * getting all todos
     */
    public List<Todo> getAllTodos(){
        List<Todo> list = new ArrayList<>();
        String selectQ = "SELECT * FROM "+ TABLE_TODO;

        Log.e(LOG, selectQ);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery(selectQ,null);

        //looping through all rows and adding to list
        if(cur.moveToFirst()){
            do{
                Todo td = new Todo();
                td.setId(cur.getInt(cur.getColumnIndex(KEY_ID)));
                td.setNote(cur.getString(cur.getColumnIndex(KEY_TODO)));
                td.setCreated_at(cur.getString(cur.getColumnIndex(KEY_CREATED_AT)));

                //adding to todo list
                list.add(td);

            } while (cur.moveToNext());
        }

        return list;
    }

    /**
     * select all todos under a tag name
     *
     * QUERY:
     * SELECT * FROM todos td, tags tg, todo_tags tt WHERE tg.tag_name =
     * ‘Watchlist’ AND tg.id = tt.tag_id AND td.id = tt.todo_id;
     */
    public List<Todo> getAllTodosByTag(String tag_name){
        List<Todo> list = new ArrayList<Todo>();

        String selectQ = "SELECT  * FROM " + TABLE_TODO + " td, "
                + TABLE_TAG + " tg, " + TABLE_TODO_TAG + " tt WHERE tg."
                + KEY_TAG_NAME + " = '" + tag_name + "'" + " AND tg." + KEY_ID
                + " = " + "tt." + KEY_TAG_ID + " AND td." + KEY_ID + " = "
                + "tt." + KEY_TODO_ID;

        SQLiteDatabase db = this.getReadableDatabase();

        Log.e(LOG, selectQ);
        Cursor cur = db.rawQuery(selectQ, null);

        //looping through all rows
        if(cur.moveToFirst()){
            do{
                Todo td = new Todo();
                td.setId(cur.getInt(cur.getColumnIndex(KEY_ID)));
                td.setNote(cur.getString(cur.getColumnIndex(KEY_TODO)));
                td.setCreated_at(cur.getString(cur.getColumnIndex(KEY_CREATED_AT)));

                list.add(td);

            } while (cur.moveToNext());
        }

        return list;
    }

    /**
    *getting todo count
    * */
    public int getTodoCount(){
        String countQ = "SELECT * FROm " + TABLE_TODO;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery(countQ, null);

        int count = cur.getCount();
        cur.close();

        return count;
    }


    /**
     *
     * @param todo
     * @return int
     */
    public int updateTodo(Todo todo){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues val = new ContentValues();
        val.put(KEY_TODO, todo.getNote());
        val.put(KEY_STATUS, todo.getStatus());

        return db.update(TABLE_TODO, val, KEY_ID + " = ? ",
                new String[]{ String.valueOf(todo.getId())});
    }

    /**
     * deleting a todo
     */
    public void deleteTodo(long todo_id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TODO, KEY_ID + " = ? ", new String[]{String.valueOf(todo_id)});
    }

    /**
     * creating a tag
     */
    public long createTag(Tag tag){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();
        val.put(KEY_TAG_NAME, tag.getTag_name());
        val.put(KEY_CREATED_AT, getDateTime());

        //insert row
        long tag_id = db.insert(TABLE_TAG, null, val);

        return tag_id;
    }

    /**
     * getting all tags
     */
    public List<Tag> getAllTags(){
        List<Tag> list = new ArrayList<Tag>();
        String selectQ = "SELECT * FROM "+TABLE_TAG;

        Log.e(LOG, selectQ);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur =db.rawQuery(selectQ, null);

        if(cur.moveToFirst()){
            do {
                Tag t = new Tag();
                t.setTag_name(cur.getString(cur.getColumnIndex(KEY_TAG_NAME)));
                t.setId(cur.getInt(cur.getColumnIndex(KEY_ID)));

                list.add(t);
            } while (cur.moveToNext());
        }
        return list;
    }

    /**
     * updating a tag
     */

    public int updateTag(Tag tag){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues val = new ContentValues();
        val.put(KEY_TAG_NAME, tag.getTag_name());

        return db.update(TABLE_TAG, val, KEY_ID + " = ? ",
                new String[]{ String.valueOf(tag.getId())});
    }

    /**
     * deleting a tag
     */
    public void deleteTag(Tag tag, boolean should_delete_all_tag_todos){
        SQLiteDatabase db = this.getWritableDatabase();

        //before deleting tag
        //check if todos under this tag should be deleted
        if(should_delete_all_tag_todos){
            //get all todos under this tag
            List<Todo> allTagTodos = getAllTodosByTag(tag.getTag_name());

            //delete all todos
            for (Todo td: allTagTodos ) {
                deleteTodo(td.getId());
            }
        }

        //now delete the tag
        db.delete(TABLE_TAG, KEY_ID + " = ? ", new String[]{ String.valueOf(tag.getId())});
    }

    /**********************************************
     * todo_tag access functions
     */

    /**
     * creating a todo_tag
     */
    public long createTodoTag(long todo_id, long tag_id){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues val = new ContentValues();
        val.put(KEY_TODO_ID, todo_id);
        val.put(KEY_TAG_ID, tag_id);
        val.put(KEY_CREATED_AT, getDateTime());

        long id = db.insert(TABLE_TODO_TAG, null, val);
        return id;
    }

    /**
     * updating a todo_tag
     */
    public int updateNoteTag(long id, long tag_id){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues val = new ContentValues();
        val.put(KEY_TAG_ID, tag_id);

        return db.update(TABLE_TODO, val, KEY_ID + " = ? ", new String[]{ String.valueOf(id)});
    }

    /**
     * deleting a todo_tag
     */
    public void deleteTodoTag(long id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TODO, KEY_ID + " = ? ",new String[]{String.valueOf(id)});
    }

    /**
     *closing Db
     */
    public void closeDb(){
        SQLiteDatabase db = this.getReadableDatabase();
        if(db != null && db.isOpen()){
            db.close();
        }
    }

}
