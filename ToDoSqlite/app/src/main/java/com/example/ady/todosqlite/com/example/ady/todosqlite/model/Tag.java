package com.example.ady.todosqlite.com.example.ady.todosqlite.model;

/**
 * Created by ady on 05.02.2016.
 */
public class Tag {
    int id;
    String tag_name;

    public Tag(){

    }

    public Tag(String tag_name){
        this.tag_name = tag_name;
    }

    public Tag(int id, String tag_name){
        this.id = id;
        this.tag_name = tag_name;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setTag_name(String tag_name){
        this.tag_name = tag_name;
    }

    public int getId(){
        return this.id;
    }

    public String getTag_name(){
        return this.tag_name;
    }

}
