package com.mamlambo.examples.easygridlayout;

import android.app.Activity;
import android.os.Bundle;

public class EasyGridLayoutActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.number_pad);
    }
}